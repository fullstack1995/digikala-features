<?php
/*
Plugin Name: update checker
Version: 1.2
*/

require 'plugin-update-checker-4.11/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/fullstack1995/digikala-features',
	__FILE__,
	'update-checker'
);

//Set the branch that contains the stable release.
$myUpdateChecker->setBranch('master');

//Optional: If you're using a private repository, specify the access token like this:
$myUpdateChecker->setAuthentication('glpat-6Cc45s8EDR-58ySMpyHx');

// im v1.2


